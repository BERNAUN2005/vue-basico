new Vue({
    el: '#app',
    
    data () {
      return {
        courses: [],
        newCourse: {
          title: '',
          time: 0
         },
         courseTitleIsValid: true,
        timeIsValid: false
       }
    },
    
    computed: {
      totalTime: function () { 
          if (this.courses.lenght === 0) { 
              return 0 
            }
            let total = 0 
            this.courses.forEach((course) => { total = parseInt(total) + parseInt(course.time) }) 
            return total 
        }
    }, 
    
    methods: {
      addCourse() {
        if (this.newCourse.title.length == 0) {
          this.courseTitleIsValid = false
          return
         }
  
  
         let newTime = parseInt(this.newCourse.time)
  
         if (newTime < 5) {
           this.timeisValid = false
           return
         }
  
         this.course.push(this.newCourse)
         this.newCourse = {
           title: '',
           time: 0
         }
         this.courseTitleIsValid = true
         this.timeIsValid = true
  
         this.$refs.title.focus();
       }
    }
  })